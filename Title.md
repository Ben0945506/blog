### Welcome to my Blog!
---

Mijn naam is Ben ik doe CMD (Communicatie, multi-media en design) aan de
Hogeschool Rotterdam. 

**Day 1, 04-09-2017**

> Het Team waar ik in zit heet "team infinity" Het staat voor oneindig vriendschap, ideeën en positieve vibes. Ik ben mijn team tijdens de introductie week tegen gekomen en we konden het goed met elkaar vinden. Dat blijkt ook tijdens onze eerste les samen. Meteen hadden we allemaal ideeën over de mogelijke games die we konden maken, en toen we de perfecte game hadden (nog steeds dag één) gingen we lunchen bij Switie (een Surinaamse broodjes tent aan het begin van de Witte de With straat) waar nog meer ideeën gegenereerd werden, die we meteen in onze Dummie schreven. 

**Day 2, 05-09-2017**
>Op dag 2 hadden we een hoorcollege over games, ik vond dit een erg interessant en leuk hoorcollege. Met een goede powerpoint en een leuke spreker. Ik wist meer over het maken van prototypes en verbeteren door middel van het testen van de prototypes. Na dit hoorcollege had ik veel meer ideeën voor de game die we al hadden bedacht.
>Ik had alles opgeschreven in mijn Dummie (een notebook), dat hielp mij met het onthouden van deze ideeën. Na het hoorcollege was de dag al weer voorbij en ging naar huis denkend aan onze game, en bedacht nog wat aanpassingen voor de app.
 
**Day 3, 06-09-2017**

>Op de derde dag begonnen we meteen met het designen van onze prototypes, waardoor we veel hebben verandert en verbeterd. We hadden de taken goed verdeelt. Ik en Fabian begonnen met het maken van onze Team Logo.![](https://lh3.googleusercontent.com/-0JQ38fXCETQ/WbZHxv_Px6I/AAAAAAAAAHk/5b202jimjh8593KgxT3W2Y6tApRgO0IvgCLcBGAs/s0/IMG-20170905-WA0001.jpg "IMG-20170905-WA0001.jpg")
>Terwijl Mark en Aaron waren begonnen aan het maken van de digitale prototypes van onze game (een app). Die er al erg mooi uitzagen. Die dag hadden we veel afgemaakt. 

**Day 4, 07-09-2017**

>Vandag (donderdag) hadden we een lekker kort dagje, met een hoorcollege over de "Design challenge" pas om 15:00. Daar moesten we een situatie gaan organiseren door vragen te stellen vanuit de "gebruikers" visie. Wat hadden de eerste poging veel goed maar ook een aantal foutjes waar de docent ons bij hielp, wat erg handig was.
>En de dag was al weer voorbij om 16:30.

**Day 5, 11-09-2017**

> Written with [StackEdit](https://stackedit.io/).9