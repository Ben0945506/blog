---
title: Ben's Blog
date: 2017-04-09
tags: ["blog", "studentlife", "fun"]

---




### Welcome to my Blog!!!
---

### Mijn naam is Ben Lim, ik doe CMD (Communicatie, multi-media en design) aan de Hogeschool Rotterdam. 

---

### Dit is Summer school, ik moet 4 competenties herkansen om mijn P te halen en dit gaat mij zeker lukken.

**Day 1, 18-6-2018**

> Vandaag hebben wij de porject informatie gekregen en weten wat wij moete gaan doen. We moeten een interactief fietsverhuur bedrijf maken. We mogen zelf de doelgroep kiezen en de locatie. Dit maakt het voor mij al een stuk aantrekkelijker, ik kan dan een stuk creatiever nadenken en onderzoekn. Ik heb vandaag dan ook meteen deskresearch gedaan om snel concept ideeëm op te wekken.

**Day 2, 19-6-2018**

> Vandaag is de echte eerste dag die is vol zit met maken, maken en nog een maken, ik heb vandaag mijn concept bedacht en feedback ervoor gevraagd aan Mieke, deze was erg positief en ga er dus mee door. Ook heb ik gemaakt: een onderzoeksplan, het ontwerpdoel en een verbredend onderzoek. Ik ben bekaf maar ben blij dat ik het allemaal heb gemaakt. Morgen moet ik heel de dag gaan odnerzoeken. Ik ben goed voorbereid en verwacht goede reacties.

**Day 3, 20-6-2018**

> Vandaag is het onderzoek tijd! We moeten onderzoek gaan doen naar onze doelgroep en de locatie. En uit dit onderzoek een onderzoeksposter maken om de onderzoeks resultaten te visualiseren. Omdat ik Verbeelden en uitwerken al heb behaald stop ik niet al te veel energie in de visuele aspecten van de Poster maar zorg ik juist voor beter kwaliteit van de inhoud. Ik had feedback gekregen en daar kwam eigenlij al uit wat ik dacht. De inhoud is goed en het onderzoek was goed verwerkt. Alleen visueel was het niet helemaal in orde, maar Mieke zij ook dat ik me daar niet al te veel zorgen om moest maken. 

**Day 4, 21-6-2018**

> Vandaag was het doel om veel creatieve technieken te gebruiken om zo je concept de creeëren of je bestaande concept te verbeteren. Ik was samen met Aaron en we hebben 6 creatieve technieken toegepast, uit deze creatieve technieken heb ik mijn concept kunnen verbeteren. En een aantal dingen kunnen teovoegen om het concept wat beter te maken. Ook had ik Aaron goed geholpen om zijn concept te verbeteren. Toen dit eenmaal klaar was (na 2.5 uur) ben ik begonnen met het maken van de concept poster. Deze poster wilde ik wel visueel aantrekklijk maken, omdat ik aan de hand van deze poster mensen ga interviewen wat ze precies van het concept vinden en wat ze zouden veranderen. Of te wel feedback vragen. 

**Day 5, 22-6-2018**'

> Vandaag ben ik verder gegaan met het maken van de concept poster, zoals ik al zij wilde ik deze poster graag visueel mooi maken. En probeerde alle belangrijke elementen van mijn concept in de poster te stoppen. Zelf vind ik dat dit goed gelukt was toen ik klaar was, alle belangrijke informatie pver het gebruik van het huur systeem en een aantal weetjes staan in de poster. Zo kan ik erg makkelijk mijn concept pitchen bij mijn volgende odnerzoek!

**Day 6 en 7, 23 en 24-6-2018**

> In het weekend heb ik verdere deskresearch gedaan naar de concept veranderingen van de creatieve sessies. Ook heb ik ervoor gezorgd dat al mijn bewijs matteriaal vast staat. Zodat ik volgende week bij het maken van mijn leerdossier alles erbij kan zetten, om de competenties zonder twijfel te behalen!

**Day 8, 25-6-2018**

> Vandaag was een enorm drukke dag met heel veel maak werk en voorbereidingen voor het 2e onderzoek. Ik heb vandaag een User journey toekomstig gemaakt en hier feeback op gekregen. Ook heb ik mijn concept poster verbeterd aan de hand van de feedback. Ik heb een testplan geschreven geheel met hypothese. ik heb al mijn meteriaal voor het onderzoek uitgeprint en heb de besloten om half 9 op centraal station te staan. Ook heb ik mijn low-dif protoype vandaag gemaakt. Dit allemaal in een dag, zucht. Maar het moet gebeuren, ik doe het namelijk niet voor niets. 

**Day 9, 26-6-2018**

> Vandaag is het dinsdag en we moeten weer is hard aan de bak! Het begon allemaal met het opnieuw doen van een onderzoek. Deze keer moesten wij het low-fid prototype testen. Dit deed ik op de locatie van mijn concept en het ging enorm goed. Iedereen was erg enthousiast. En ook kreeg ik meer inzicht over de doelgroepen! Ik was vergeten de teoristen doelgroep toe te passen aan mijn concept. Terwijl zij dit juist zouden willen gebruiken (komt uit het onderzoek). Dus dat was er handig om te weten! Voor de rest van de dag heb ik mijn onderzoeks resultaten in een test reportage gezet (een vrij uitgebreid test raportage). En in de middag heb ik de visuele style guide en Merk analyse gemaakt!

**Day 10, 27-6-2018**

> Vandaag moeten wij het high-fid protoype maken en deze testen. Ik doe mijn high-dif in de vorm van een app. Ik focus me vooral op de inhoud i.p.v. de vormgeveing aangezien ik deze competentie al heb behaald. Ook moet ik een design rationale maken. Ook moet ik mijn presentatie voorbereiden voor het Shine moment waar ik mij voor heb ingeschreven. Na dit shine moment krijg ik feedback over mijn high-fid en kan dit gebruiken voor de competentie evalueren ontwerp resultaten!

**Day 11, 28-6-2018**

> Vandaag heb ik mijn 3e en laatste onderzoek gedaan. Ik heb bij het "Shine" moment mijn concept en high-fid getest met 4 mensen. Hieruit heb ik veel feedback gekregen en heb dit kunnen gebruiken om een deel van een STARRT te schrijven. Ook is het vandaag de laatste dag en dus ook de laatste blog Post :(. 

###Samenvating Summer School:

---

Van begin tot eind was dit een stressvolle maar enorm leerzame ervaring. Ik ben deels blij dat ik dit heb gevolgd heb omdat ik hier wel heel van van heb geleerd! Ook vond ik het project erg leuk en was trots op wat ik had geleverd. Dit is wel helaas mijn laatste jaar hier aangezien ik hier helaas weg ga. Maar ik wilde perse mijn Propedeuse halen. En heb hiervoor alles gegeven deze 2 weken!
